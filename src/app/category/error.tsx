"use client";

import { useEffect } from "react";

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    console.error("error", JSON.stringify(error));
  }, [error]);

  return (
    <div className="font-sans h-screen text-center flex flex-col items-center justify-center">
      <div className="flex items-center justify-center">
        <h1 className="next-error-h1 inline-block m-0 mr-20 pr-23 text-2xl font-semibold align-top leading-12">
          Something went wrong
        </h1>
        <div className="inline-block">
          <button
            className={`bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded`}
            onClick={reset}
          >
            Retry
          </button>
        </div>
      </div>
    </div>
  );
}
