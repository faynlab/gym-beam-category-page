import { ProductItem, Sidebar } from "@components";
import { ApiMethod } from "@constants";
import { IProductListResponse } from "@models";
import { callApi } from "@services";

export default async function CategoryPage({
  searchParams,
}: {
  searchParams: { [key: string]: string };
}) {
  const queryParams = { "category_ids[]": "2416", ...searchParams };

  const { items, filters } = await callApi<IProductListResponse>(
    ApiMethod.GET,
    "catalog/products",
    queryParams
  );

  return (
    <div className="container mx-auto my-8">
      <div className="flex flex-col md:flex-row">
        <Sidebar filters={filters} searchParams={searchParams} />
        <div className="flex-1 md:order-2">
          <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
            {items.map((product) => (
              <ProductItem key={product.id} product={product} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
