import { IFilter } from "./IFilter";
import { IProduct } from "./IProduct";

export interface IProductListResponse {
  items: IProduct[];
  filters: IFilter[];
}
