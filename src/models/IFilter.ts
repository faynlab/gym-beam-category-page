import { IFilterOption } from "./IFilterOption";

export interface IFilter {
  name: string;
  code: string;
  global_name: string;
  display_mode: string;
  type: "multiselect" | "checkbox";
  position: string;
  options: IFilterOption[];
}
