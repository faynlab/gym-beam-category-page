export interface IFilterOption {
  name: string;
  slug: string;
  value: string;
  count: number;
}
