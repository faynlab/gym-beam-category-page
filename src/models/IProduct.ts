export interface IProduct {
  id: number;
  name: string;
  price: number;
  formatted_price: string;
  image: string;
  thumbnail: string;
  small_image: string;
  reviews_count: number;
  rating_summary: number;
}
