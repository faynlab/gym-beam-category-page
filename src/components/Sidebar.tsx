"use client";

import { useState } from "react";
import { FilterMultiSelect } from "./FilterMultiSelect";
import { IFilter } from "@models";
import React from "react";

interface IProps {
  filters: IFilter[];
  searchParams: { [key: string]: string };
}

export const Sidebar: React.FC<IProps> = ({ filters, searchParams }) => {
  const [showFilters, setShowFilters] = useState(false);

  const toggleFilters = () => {
    setShowFilters((prev) => !prev);
  };

  return (
    <>
      <button
        className="md:hidden bg-gray-200 p-4 w-full text-left"
        onClick={toggleFilters}
      >
        {showFilters ? "Hide Filters" : "Show Filters"}
      </button>

      <div
        className={`bg-gray-200 p-4 md:order-1 md:w-1/4 ${
          showFilters ? "block" : "hidden md:block"
        }`}
      >
        <div className="text-xl font-semibold mb-4">Filters</div>

        {filters.map((filter) => {
          return filter.type === "multiselect" ? (
            <FilterMultiSelect
              key={filter.code}
              code={filter.code}
              label={filter.name}
              options={filter.options}
              searchParams={searchParams}
            />
          ) : null;
        })}
      </div>
    </>
  );
};
