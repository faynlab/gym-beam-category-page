"use client";

import { useRouter } from "next/navigation";
import React, { useState } from "react";

export interface MultiSelectOption {
  name: string;
  value: string;
}

interface IProps {
  label: string;
  code: string;
  options: MultiSelectOption[];
  searchParams: { [key: string]: string };
}

export const FilterMultiSelect: React.FC<IProps> = React.memo(
  ({ label, options, code, searchParams }) => {
    const router = useRouter();
    const [showOptions, setShowOptions] = useState(false);

    const toggleOptions = () => {
      setShowOptions((prev) => !prev);
    };

    const searchParamsArray: string[] | undefined =
      searchParams[`${code}[]`]?.split(",");

    const selectedOptions = options?.filter(
      ({ value }) => searchParamsArray?.includes(value)
    );

    const handleFilterChange = (newValue: MultiSelectOption[]) => {
      const newSearchParams = {
        ...searchParams,
        [`${code}[]`]: newValue.reduce(
          (prev, curr, index) => prev + (index > 0 ? "," : "") + curr.value,
          ""
        ),
      };

      const searchParamsString = Object.entries(newSearchParams)
        .filter(([_, value]) => value !== "")
        .map(
          ([key, value]) =>
            `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
        )
        .join("&");

      router.push(`/category?${searchParamsString}`);
    };

    return (
      <div className="mb-4">
        <label className="block text-sm font-medium text-gray-600">
          {label}
        </label>
        <button
          className="text-blue-500 underline mb-2"
          onClick={toggleOptions}
        >
          {showOptions ? "Hide Options" : "Show Options"}
        </button>
        {showOptions ? (
          <div>
            {options.map((option) => {
              const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
                if (e.target.checked)
                  handleFilterChange([...selectedOptions, option]);
                else
                  handleFilterChange(
                    selectedOptions.filter((val) => val.value !== option.value)
                  );
              };

              return (
                <label key={option.value} className="flex items-center">
                  <input
                    type="checkbox"
                    value={option.value}
                    checked={
                      !!selectedOptions.find(
                        ({ value: propsValue }) => propsValue === option.value
                      )
                    }
                    onChange={handleChange}
                    className="mr-2"
                  />
                  {option.name}
                </label>
              );
            })}
          </div>
        ) : null}
      </div>
    );
  }
);
