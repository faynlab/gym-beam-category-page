import { IProduct } from "@models";
import Image from "next/image";

interface IProps {
  product: IProduct;
}

export const ProductItem: React.FC<IProps> = ({ product }) => {
  return (
    <div key={product.id} className="bg-white p-6 rounded-lg shadow-md">
      <Image
        src={product.thumbnail}
        alt={product.name}
        width={1200}
        height={1200}
        loading="lazy"
      />
      <h2 className="text-xl font-semibold mb-2">{product.name}</h2>
      <p className="text-gray-600 mb-2">{product.price}</p>
      <div className="flex items-center mb-2">
        <div>Rating:</div>
        <span className="ml-1">{product.rating_summary}%</span>
      </div>
    </div>
  );
};
