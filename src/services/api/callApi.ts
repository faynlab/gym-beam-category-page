import { ApiMethod, QueryParams } from "@constants";

const baseURL = "https://gymbeam.sk/rest/V1/gb/";

export const callApi = async <T>(
  method: ApiMethod,
  route: string,
  queryParams?: QueryParams
): Promise<T> => {
  try {
    const url = new URL(route, baseURL);

    if (queryParams) {
      Object.keys(queryParams).forEach((key) => {
        url.searchParams.append(key, queryParams[key]);
      });
    }

    const response = await fetch(url.href, {
      method,
      headers: {
        "content-type": "application/json",
      },
    });

    if (!response.ok) {
      throw response.status;
    }

    return response.json();
  } catch (error) {
    throw error;
  }
};
